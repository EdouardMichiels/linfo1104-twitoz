functor
import 
   QTk at 'x-oz://system/wp/QTk.ozf'
   System
   Application
   Open
   OS
   Property
   Browser
define
   %On definit ici les variables globales
   InputText 
   OutputText1 OutputButton1
   OutputText2 OutputButton2
   OutputText3 OutputButton3
   PredButton 
   SwitchButton 
   Description 
   Window 
   BigBox Box1 Box2 Box3 C1 C2 C3 C4 C5 C6 C7 C8 C9 C10 C11 C12
   P1 P2 P3 
   SeparatedWordsStream Data 
   ColorMode ColorIcon 
   CorrectionButton CorrectionText 
   Global 

   %%% Pour ouvrir les fichiers
   class TextFile
      from Open.file Open.text
   end

   proc {Browse Buf}
      {Browser.browse Buf}
   end
   
   %%% /!\ Fonction testee /!\
   %%% @pre : les threads sont "ready"
   %%% @post: Fonction appellee lorsqu on appuie sur le bouton de prediction
   %%%        Affiche la prediction la plus probable du prochain mot selon les deux derniers mots entres
   %%% @return: Retourne une liste contenant la liste du/des mot(s) le(s) plus probable(s) accompagnee de 
   %%%          la probabilite/frequence la plus elevee. 
   %%%          La valeur de retour doit prendre la forme:
   %%%                  <return_val> := <most_probable_words> '|' <probability/frequence> '|' nil
   %%%                  <most_probable_words> := <atom> '|' <most_probable_words> 
   %%%                                           | nil
   %%%                  <probability/frequence> := <int> | <float>
   fun {Press}
      local Var Prediction in
         Var = {GetLastWords {InputText getText(p(1 0) 'end' $)}}
         case Var
         of [Word NextWord] then
            Prediction = {Lookup Var.2.1 {Lookup Var.1 Data}}
         [] _ then Prediction = nil
         end
         case Prediction
         of nil then nil
         [] leaf then nil
         [] _ then {SortList {DFS Prediction nil} nil}
         end
      end
   end
   
   %%% Lance les N threads de lecture et de parsing qui liront et traiteront tous les fichiers
   %%% Les threads de parsing envoient leur resultat au port Port
   proc {LaunchThreads Port N}
   proc {ThreadsCreation Port N CurrentThread NbFiles TweetsFolder Tweets}
      proc {TheadsReadNFiles CurrentFile MaxFile TweetsFolder Tweets ?R}
         fun{TheadsReadNFilesAcc CurrentFile MaxFile TweetsFolder Tweets Acc}
            if CurrentFile>MaxFile then Acc
            else
               {TheadsReadNFilesAcc CurrentFile+1 MaxFile TweetsFolder Tweets {Scan {New TextFile init(name: TweetsFolder#"/"#{List.nth Tweets CurrentFile})} Acc}}
            end
         end
      in
         R = {TheadsReadNFilesAcc CurrentFile MaxFile TweetsFolder Tweets nil}
      end
      proc {ThreadsParser P S}
         case S of nil then {Send P done}
         [] H|T then {Send P {SplitLine H}} {ThreadsParser P T}
         end
      end
      S
   in
      if CurrentThread =< N then
         thread
            S = {TheadsReadNFiles 1 {List.length {List.nth Tweets CurrentThread}} TweetsFolder {List.nth Tweets CurrentThread}}
         end
         thread {ThreadsParser Port S} end
         {ThreadsCreation Port N CurrentThread+1 NbFiles TweetsFolder Tweets}
      end
   end
   in
      local TweetsFolder Tweets L Out in
         TweetsFolder = {GetSentenceFolder}
         Tweets = {OS.getDir TweetsFolder}
         L = {SplitIntoN Tweets N nil 1 {List.length Tweets}}
         {ThreadsCreation Port N 1 {List.length Tweets} TweetsFolder L}
      end
   end
   
   %%%% Ajouter vos fonctions et procedures auxiliaires ici

   %% Procedure qui permet de separer les mots d'une phrase
   proc {SplitLine Sentence ?R}
      fun {F String Liste Words} 
         case String
         of nil then
            if Words == nil then Liste else {Append Liste [Words]} end
         [] H|T then
            if (H == 226) andthen T.1 == 128 then
               if Words == nil then {F T Liste nil}
               elseif H == 8217 then {F T Liste Words}
               else {F T {Append Liste [Words]} nil}
               end
            else
               case {Char.type H}
               of lower then {F T Liste {Append Words [H]}}
               [] upper then {F T Liste {Append Words [{Char.toLower H}]}}
               [] digit then {F T Liste {Append Words [H]}}
               [] punct then
                  if H == 38 then {F T.2.2.2.2 Liste nil}
                  else
                     if Words == nil then {F T {Append Liste [nil]} nil}
                     else
                        {F T {Append {Append Liste [Words]} [nil]} nil}
                     end
                  end
               else
                  if Words == nil then {F T Liste nil}
                  elseif H == 8217 then {F T Liste Words}
                  else {F T {Append Liste [Words]} nil}
                  end
               end
            end
         else
            nil
         end
      end
   in 
      R = {F Sentence nil nil}
   end

   %%% Fonction qui permet de lire les fichiers et d'en extraire les lignes
   fun {Scan IF Out}
      Is={IF getS($)}  
   in 
      if Is==false then
         {IF close}
         Out
      else 
         {Scan IF {List.append Out [Is]}}
      end 
   end 

   %Fonction qui permet de séparer une liste en listes de N éléments
   fun {SplitIntoN L N Acc I Len}
      if I == N then {List.append Acc [L]}
      else
            local A B in
               {List.takeDrop L Len div N A B }
               {SplitIntoN B N {List.append Acc [A]} I+1 Len}
            end
      end
   end

   %% Procedure qui permet de recuperer les resultats des threads de parsing et les transforme en arbre
   proc {StreamToTree S NbThreads ?R}
      fun {StreamToTreeAcc S Tree NbThreads NbFinished}
         case S
         of H|T then
            if H == done then
               if NbFinished == NbThreads then Tree
               else {StreamToTreeAcc T Tree NbThreads NbFinished+1}
               end
            else {StreamToTreeAcc T {AddToTree Tree H} NbThreads NbFinished}
            end
         end  
      end
   in 
      R = {StreamToTreeAcc S leaf NbThreads-1 0}
   end

   %% Fonction qui permet d'ajouter un mot a l'arbre
   fun {AddToTree Tree List}
      proc {AddWord Tree Word NextWord Predictable ?R}
         fun {StrToAtom Word} if {Atom.is Word} then Word else {String.toAtom Word} end end
         fun {F Tree Word NextWord Predictable}
            if Word == nil orelse NextWord == nil orelse Predictable == nil then Tree
            else 
               local A1 A2 in
                  A1 = {Lookup Word Tree}
                  case A1 
                  of leaf then 
                     A2 = {Insert Word {Insert NextWord {Insert Predictable 1 leaf} leaf} Tree}
                     A2
                  [] tree(key:K value :V T1 T2) then
                     local B1 B2 B3 in
                        B1 = {Lookup NextWord A1}
                        case B1
                        of leaf then
                           B2 = {Insert NextWord {Insert Predictable 1 leaf} A1}
                           B3 = {Insert Word B2 Tree}
                           B3
                        [] tree(key:K value :V T1 T2) then
                           local C1 C2 C3 C4 in
                              C1 = {Lookup Predictable B1}
                              if C1 == leaf then
                                 C4 = {Insert Predictable 1 B1}
                                 C3 = {Insert NextWord C4 A1}
                                 C2 = {Insert Word C3 Tree}
                                 C2
                              else 
                                 C4 = {Insert Predictable {String.toInt {VirtualString.toString C1}}+1 B1}
                                 C3 = {Insert NextWord C4 A1}
                                 C2 = {Insert Word C3 Tree}
                                 C2
                              end
                           end
                        end
                     end
                  end
               end
            end
         end
      in
         R = {F Tree {StrToAtom Word} {StrToAtom NextWord} {StrToAtom Predictable}}
      end
   in
      case List
      of nil then Tree
      [] Word|T then
         case T of nil then Tree
         [] NextWord|T then
            case T of nil then Tree
            [] Predictable|_ then {AddToTree {AddWord Tree Word NextWord Predictable} List.2}
            end
         end
      end
   end

   %% Fonction qui retourne les 2 derniers mots d'une liste de mots
   proc {GetLastWords L ?R}
      fun {GetLast2 L}
         if L == nil then nil
         elseif {List.length L} == 1 then nil
         else [{String.toAtom L.2.1} {String.toAtom L.1}]
         end
      end 
   in
      R = {GetLast2 {List.reverse {SplitLine L}}}
   end

   %% Fonction qui ajoute une valeur a un arbre
   fun {Insert K W T}
      case T
      of leaf then tree(key:K value:W leaf leaf)
      [] tree(key:Y value:V T1 T2) andthen K==Y then
          tree(key:K value:W T1 T2)
      [] tree(key:Y value:V T1 T2) andthen K<Y then
          tree(key:Y value:V {Insert K W T1} T2)
      [] tree(key:Y value:V T1 T2) andthen K>Y then
          tree(key:Y value:V T1 {Insert K W T2})
      end
   end
   
   %% Fonction qui retourne la valeur associee a une cle dans un arbre
   fun {Lookup K T}
      case T
      of leaf then leaf
      [] tree(key:Y value:V T1 T2) andthen K==Y then V
      [] tree(key:Y value:V T1 T2) andthen K<Y then {Lookup K T1}
      [] tree(key:Y value:V T1 T2) andthen K>Y then {Lookup K T2}
      end
   end

   %% Fonction qui parcours un arbre en profondeur et retourne une liste de listes de mots et de leur probabilite
   fun {DFS T Acc}
      case T
      of leaf then Acc
      [] tree(key:K value:V T1 T2) then
          local X Y Z in
              X = {List.append Acc [[[K] V]]}
              Y = {DFS T1 X}
              Z = {DFS T2 Y}
              Z
          end
      end
   end
   

   %%Fonction qui retourne une liste avec comme premier élément le mot le moin probables et comme deuxième élément le reste de la liste
   fun {GetLastProabableWord L Acc R}
      case L
      of nil then Acc|R
      [] H|T then
         if {String.toInt {VirtualString.toString H.2.1}} < {String.toInt {VirtualString.toString Acc.2.1}} then {GetLastProabableWord T H Acc|R}
         
         elseif {String.toInt {VirtualString.toString H.2.1}} == {String.toInt {VirtualString.toString Acc.2.1}} then 
            if {VirtualString.toString H.1.1}.1 > {VirtualString.toString Acc.1.1}.1 then {GetLastProabableWord T H Acc|R}
            else {GetLastProabableWord T Acc H|R} end 
          
         else {GetLastProabableWord T Acc H|R} end
      end
   end

   %%Fonction qui retourne la liste triée dans l'ordre des probabilités
   fun {SortList L Acc}
      local M in
         case L
         of nil then M = nil
         [] H|T then M = {GetLastProabableWord T H nil} end
         case M
         of nil then Acc
         [] H|T then {SortList T H|Acc} end
      end
   end


   %% Procedure qui declenche le changement de mode de couleur
   proc {Switch}
      if({ColorMode get($)}=="Light") then
         {OutputText1 set(bg:dimgray)}
         {OutputText1 set(foreground:white)}
         {OutputText2 set(bg:dimgray)}
         {OutputText2 set(foreground:white)}
         {OutputText3 set(bg:dimgray)}
         {OutputText3 set(foreground:white)}
         {CorrectionText set(bg:dimgray)}
         {CorrectionText set(foreground:white)}
         {CorrectionButton set(bg:black)}
         {CorrectionButton set(foreground:white)}
         {InputText set(bg:dimgray)}
         {InputText set(foreground:white)}
         {PredButton set(bg:black)}
         {PredButton set(foreground:white)}
         {OutputButton1 set(bg:black)}
         {OutputButton1 set(foreground:white)}
         {OutputButton2 set(bg:black)}
         {OutputButton2 set(foreground:white)}
         {OutputButton3 set(bg:black)}
         {OutputButton3 set(foreground:white)}
         {C1 set(bg:black)}
         {C2 set(bg:black)}
         {C3 set(bg:black)}
         {C4 set(bg:black)}
         {C5 set(bg:black)}
         {C6 set(bg:black)}
         {C7 set(bg:black)}
         {C8 set(bg:black)}
         {C9 set(bg:black)}
         {C10 set(bg:black)}
         {C11 set(bg:black)}
         {C12 set(bg:black)}
         {P1 set(bg:black)}
         {P1 set(foreground:white)}
         {P2 set(bg:black)}
         {P2 set(foreground:white)}
         {P3 set(bg:black)}
         {P3 set(foreground:white)}
         {Global set(bg:black)}
         {Box1 set(bg:black)}
         {Box2 set(bg:black)}
         {Box3 set(bg:black)}
         {BigBox set(bg:black)}
         {ColorMode set(1:"Dark")}
         {ColorMode set(foreground:black)}
         {ColorMode set(bg:black)}
         {Window set(bg:black)}
      else
         {OutputText1 set(bg:white)}
         {OutputText1 set(foreground:black)}
         {OutputText2 set(bg:white)}
         {OutputText2 set(foreground:black)}
         {OutputText3 set(bg:white)}
         {OutputText3 set(foreground:black)}
         {CorrectionText set(bg:white)}
         {CorrectionText set(foreground:black)}
         {CorrectionButton set(bg:lightgray)}
         {CorrectionButton set(foreground:black)}
         {InputText set(bg:white)}
         {InputText set(foreground:black)}
         {PredButton set(bg:lightgray)}
         {PredButton set(foreground:black)}
         {OutputButton1 set(bg:lightgray)}
         {OutputButton1 set(foreground:black)}
         {OutputButton2 set(bg:lightgray)}
         {OutputButton2 set(foreground:black)}
         {OutputButton3 set(bg:lightgray)}
         {OutputButton3 set(foreground:black)}
         {C1 set(bg:lightgray)}
         {C2 set(bg:lightgray)}
         {C3 set(bg:lightgray)}
         {C4 set(bg:lightgray)}
         {C5 set(bg:lightgray)}
         {C6 set(bg:lightgray)}
         {C7 set(bg:lightgray)}
         {C8 set(bg:lightgray)}
         {C9 set(bg:lightgray)}
         {C10 set(bg:lightgray)}
         {C11 set(bg:lightgray)}
         {C12 set(bg:lightgray)}
         {P1 set(bg:lightgray)}
         {P1 set(foreground:black)}
         {P2 set(bg:lightgray)}
         {P2 set(foreground:black)}
         {P3 set(bg:lightgray)}
         {P3 set(foreground:black)}
         {Global set(bg:lightgray)}
         {Box1 set(bg:lightgray)}
         {Box2 set(bg:lightgray)}
         {Box3 set(bg:lightgray)}
         {BigBox set(bg:lightgray)}
         {ColorMode set(1:"Light")}
         {ColorMode set(foreground:lightgray)}
         {ColorMode set(bg:lightgray)}
         {Window set(bg:lightgray)}
      end
   end
   
   %% Procedure qui declenche la prediction
   proc {ActionFetch}
      local Var Len in 
         Var = {Press}
         Len = {List.length Var}
         if Len == 0 then
            {OutputText1 set(1:"/")}
            {P1 set(1:"Fréquence : /")}
            {OutputText2 set(1:"/")}
            {P2 set(1:"Fréquence : /")}
            {OutputText3 set(1:"/")}
            {P3 set(1:"Fréquence : /")}
         elseif Len == 1 then
            {OutputText1 set(1:Var.1.1.1)}
            {P1 set(1:"Fréquence : "#Var.1.2.1)}
            {OutputText2 set(1:"/")}
            {P2 set(1:"Fréquence : /")}
            {OutputText3 set(1:"/")}
            {P3 set(1:"Fréquence : /")}
         elseif Len == 2 then
            {OutputText1 set(1:Var.1.1.1)}
            {P1 set(1:"Fréquence : "#Var.1.2.1)}
            {OutputText2 set(1:Var.2.1.1.1)}
            {P2 set(1:"Fréquence : "#Var.2.1.2.1)}
            {OutputText3 set(1:"/")}
            {P3 set(1:"Fréquence : /")}
         else
            {OutputText1 set(1:Var.1.1.1)}
            {P1 set(1:"Fréquence : "#Var.1.2.1)}
            {OutputText2 set(1:Var.2.1.1.1)}
            {P2 set(1:"Fréquence : "#Var.2.1.2.1)}
            {OutputText3 set(1:Var.2.2.1.1.1)}
            {P3 set(1:"Fréquence : "#Var.2.2.1.2.1)}
         end
      end
   end

   proc {ChangeWord}
      local Word Liste ToPredict Predictables Len in
         Liste = {List.reverse {SplitLine {InputText getText(p(1 0) 'end' $)}}}
         Word = {SplitLine {CorrectionText getText(p(1 0) 'end' $)}}
         if(Word == nil) then
            {OutputText1 set(1:"/")}
            {P1 set(1:"Fréquence : /")}
            {OutputText2 set(1:"/")}
            {P2 set(1:"Fréquence : /")}
            {OutputText3 set(1:"/")}
            {P3 set(1:"Fréquence : /")}
         else
            ToPredict = {GetPrevious Liste Word.1}
            if(ToPredict == nil) then
               {OutputText1 set(1:"/")}
               {P1 set(1:"Fréquence : /")}
               {OutputText2 set(1:"/")}
               {P2 set(1:"Fréquence : /")}
               {OutputText3 set(1:"/")}
               {P3 set(1:"Fréquence : /")}
            else
               Predictables = {SortList {DFS {Lookup {VirtualString.toAtom ToPredict.1} {Lookup {VirtualString.toAtom ToPredict.2.1} Data}} nil} nil}
               Len = {List.length Predictables}
               if Len == 0 then
                  {OutputText1 set(1:"/")}
                  {P1 set(1:"Fréquence : /")}
                  {OutputText2 set(1:"/")}
                  {P2 set(1:"Fréquence : /")}
                  {OutputText3 set(1:"/")}
                  {P3 set(1:"Fréquence : /")}
               elseif Len == 1 then
                  {OutputText1 set(1:Predictables.1.1.1)}
                  {P1 set(1:"Fréquence : "#Predictables.1.2.1)}
                  {OutputText2 set(1:"/")}
                  {P2 set(1:"Fréquence : /")}
                  {OutputText3 set(1:"/")}
                  {P3 set(1:"Fréquence : /")}
               elseif Len == 2 then
                  {OutputText1 set(1:Predictables.1.1.1)}
                  {P1 set(1:"Fréquence : "#Predictables.1.2.1)}
                  {OutputText2 set(1:Predictables.2.1.1.1)}
                  {P2 set(1:"Fréquence : "#Predictables.2.1.2.1)}
                  {OutputText3 set(1:"/")}
                  {P3 set(1:"Fréquence : /")}
               else
                  {OutputText1 set(1:Predictables.1.1.1)}
                  {P1 set(1:"Fréquence : "#Predictables.1.2.1)}
                  {OutputText2 set(1:Predictables.2.1.1.1)}
                  {P2 set(1:"Fréquence : "#Predictables.2.1.2.1)}
                  {OutputText3 set(1:Predictables.2.2.1.1.1)}
                  {P3 set(1:"Fréquence : "#Predictables.2.2.1.2.1)}
               end
            end
         end
      end
   end

   fun {GetPrevious Liste Word}
      case Liste 
      of nil then nil
      [] H|T then
         if(H==Word) then 
            case T 
            of nil then nil
            [] H1|T1 then
               case T1 
               of nil then nil
               [] _ then [H1 T1.1]
               end
            end
         else
            {GetPrevious T Word}
         end
      end
   end

   proc{GetOutput1}
      local Input Output in
         Input = {InputText getText(p(1 0) 'end' $)}
         Output = {OutputText1 getText(p(1 0) 'end' $)}
         if(Output == "\n") then skip else 
            {InputText set(1:{List.take Input {List.length Input}-1}#" "#{List.take Output {List.length Output}-1})} end
      end
   end

   proc{GetOutput2}
      local Input Output in
         Input = {InputText getText(p(1 0) 'end' $)}
         Output = {OutputText2 getText(p(1 0) 'end' $)} 
         if(Output == "\n") then skip else 
            {InputText set(1:{List.take Input {List.length Input}-1}#" "#{List.take Output {List.length Output}-1})} end
      end
   end

   proc{GetOutput3}
      local Input Output in
         Input = {InputText getText(p(1 0) 'end' $)}
         Output = {OutputText3 getText(p(1 0) 'end' $)}
         if(Output == "\n") then skip else 
            {InputText set(1:{List.take Input {List.length Input}-1}#" "#{List.take Output {List.length Output}-1})} end
      end
   end

   %%% Fetch Tweets Folder from CLI Arguments
   %%% See the Makefile for an example of how it is called
   fun {GetSentenceFolder}
      Args = {Application.getArgs record('folder'(single type:string optional:false))}
   in
      Args.'folder'
   end

   %%% Decomnentez moi si besoin
   %proc {ListAllFiles L}
   %   case L of nil then skip
   %   [] H|T then {Browse {String.toAtom H}} {ListAllFiles T}
   %   end
   %end
    
   %%% Procedure principale qui cree la fenetre et appelle les differentes procedures et fonctions
   proc {Main}
      TweetsFolder = {GetSentenceFolder}
   in
      %% Fonction d'exemple qui liste tous les fichiers
      %% contenus dans le dossier passe en Argument.
      %% Inspirez vous en pour lire le contenu des fichiers
      %% se trouvant dans le dossier
      %%% N'appelez PAS cette fonction lors de la phase de
      %%% soumission !!!
      % {ListAllFiles {OS.getDir TweetsFolder}}
      
      local NbThreads SeparatedWordsPort WaitingScreen WaitingImage Waiting LoadingScreen WindowLoad in
         {Property.put print foo(width:1000 depth:1000)}  % for stdout siz
         
         % TODO
         ColorIcon = {QTk.newImage photo(url:"./images/ColorIcon.png" height:50)}
         WaitingImage = {QTk.newImage photo(url:"./images/Logo.png" width:300 height:100)}
         
         % Creation de l interface graphique	 
         Description=td(
            bg:lightgray
            title: "Text predictor"
            lr(handle:Global
            background:lightgray
            td(handle:BigBox
            label(handle:C1 width:85 height:1 background:lightgray foreground:lightgray relief:flat)
            background:lightgray
			   lr(handle:Box1 text(handle:InputText width:65 height:10 background:white foreground:black wrap:word) 
               background:lightgray
               )
            label(handle:C2 width:75 height:1 background:lightgray foreground:lightgray relief:flat)
            lr(handle:Box2
               background:lightgray
               button(handle:PredButton text:"Predict" width:10 action:ActionFetch background:lightgray foreground:black)
			      label(handle:C7 width:1 height:1 background:lightgray foreground:lightgray relief:flat)
               td(handle:C8
                  background:lightgray
                  lr(handle:C4
                  background:lightgray
                  button(handle:OutputButton1 text:">" width:1 height:1 action:GetOutput1 background:lightgray foreground:black)
                  text(handle:OutputText1 width:15 height:1 background:white foreground:black))
                  label(handle:P1 text:"" width:15 height:1 background:lightgray foreground:black relief:flat))
			      label(handle:C11 width:1 height:1 background:lightgray foreground:lightgray relief:flat)
               td(handle:C9
                  background:lightgray
                  lr(handle:C5
                  background:lightgray
                  button(handle:OutputButton2 text:">" width:1 height:1 action:GetOutput2 background:lightgray foreground:black)
                  text(handle:OutputText2 width:15 height:1 background:white foreground:black))
                  label(handle:P2 text:"" width:15 height:1 background:lightgray foreground:black relief:flat))
               label(handle:C12 width:1 height:1 background:lightgray foreground:lightgray relief:flat)
               td(handle:C10
                  background:lightgray
                  lr(handle:C6
                  background:lightgray
                  button(handle:OutputButton3 text:">" width:1 height:1 action:GetOutput3 background:lightgray foreground:black)
                  text(handle:OutputText3 width:15 height:1 background:white foreground:black))
                  label(handle:P3 text:"" width:15 height:1 background:lightgray foreground:black relief:flat))))
            td(handle:Box3
               background:lightgray
               label(handle:C3 width:15 height:1 background:lightgray foreground:lightgray relief:flat)
               text(handle:CorrectionText width:10 height:1 background:white foreground:black)
               button(handle:CorrectionButton text:"Correct" width:6 action:ChangeWord background:lightgray foreground:black)))
			   label(handle:ColorMode width:1 height:1 background:lightgray foreground:lightgray relief:flat)
            button(handle:SwitchButton image:ColorIcon width:50 height:50 action:Switch background:grey glue:e)
			   action:proc{$}{Application.exit 0} end % quitte le programme quand la fenetre est fermee
			   )
         
         Waiting = td(
            bg:lightgray
            title: "Loading..."
            label(handle:WaitingScreen image:WaitingImage width:300 height:100 background:lightgray foreground:black)
            label(handle:LoadingScreen text:"Loading data, please wait... \n \n (This should take approximately 30 seconds)" foreground:black background:lightgray height:5)
         )
         % Creation de la fenetre

	      WindowLoad={QTk.build Waiting}
         {WindowLoad show}

         % On lance les threads de lecture et de parsing
         SeparatedWordsPort = {NewPort SeparatedWordsStream}
         NbThreads = 4
         
         if(NbThreads>{List.length {OS.getDir TweetsFolder}}) then
            {LaunchThreads SeparatedWordsPort {List.length {OS.getDir TweetsFolder}}}
            Data = {StreamToTree SeparatedWordsStream {List.length {OS.getDir TweetsFolder}}}
         else 
            {LaunchThreads SeparatedWordsPort NbThreads}
            Data = {StreamToTree SeparatedWordsStream NbThreads}
         end

         {WindowLoad close}

         Window={QTk.build Description}
         {Window show}

         {ColorMode set(1:"Light")}
         
         {InputText tk(insert 'end' "Loading... Please wait.")}
         {InputText bind(event:"<Control-s>" action:ActionFetch)} % You can also bind events
         {Window bind(event:"<Control-c>" action:Switch)}
         
         {InputText set(1:"")}
      end
   %%ENDOFCODE%%
   end
   % Appelle la procedure principale
   {Main}
end