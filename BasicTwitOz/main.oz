functor
import 
   QTk at 'x-oz://system/wp/QTk.ozf'
   System
   Application
   Open
   OS
   Property
   Browser
define
   %%%% Variables Globales
   InputText OutputText Description Window SeparatedWordsStream Data

   %%%% Pour ouvrir les fichiers
   class TextFile
      from Open.file Open.text
   end

   proc {Browse Buf}
      {Browser.browse Buf}
   end
   
   %%%% /!\ Fonction testee /!\
   %%%% @pre : les threads sont "ready"
   %%%% @post: Fonction appellee lorsqu on appuie sur le bouton de prediction
   %%%%        Affiche la prediction la plus probable du prochain mot selon les deux derniers mots entres
   %%%% @return: Retourne une liste contenant la liste du/des mot(s) le(s) plus probable(s) accompagnee de 
   %%%%          la probabilite/frequence la plus elevee. 
   %%%%          La valeur de retour doit prendre la forme:
   %%%%                  <return_val> := <most_probable_words> '|' <probability/frequence> '|' nil
   %%%%                  <most_probable_words> := <atom> '|' <most_probable_words> 
   %%%%                                           | nil
   %%%%                  <probability/frequence> := <int> | <float>
   fun {Press}
      local Var Prediction in
         Var = {GetLastWords {InputText getText(p(1 0) 'end' $)}}
         case Var
         of [Word NextWord] then
            Prediction = {Lookup Var.2.1 {Lookup Var.1 Data}}
         [] _ then Prediction = leaf
         end
         case Prediction
         of leaf then [[nil] 0]
         [] _ then {GetMostProbableWord {DFS Prediction nil} [nil 0]} 
         end
      end
   end
   
   %%% Lance les N threads de lecture et de parsing qui liront et traiteront tous les fichiers
   %%% Les threads de parsing envoient leur resultat au port Port
   proc {LaunchThreads Port N}
   proc {ThreadsCreation Port N CurrentThread NbFiles TweetsFolder Tweets}
      proc {TheadsReadNFiles CurrentFile MaxFile TweetsFolder Tweets ?R}
         fun{TheadsReadNFilesAcc CurrentFile MaxFile TweetsFolder Tweets Acc}
            if CurrentFile>MaxFile then Acc
            else
               {TheadsReadNFilesAcc CurrentFile+1 MaxFile TweetsFolder Tweets {Scan {New TextFile init(name: TweetsFolder#"/"#{List.nth Tweets CurrentFile})} Acc}}
            end
         end
      in
         R = {TheadsReadNFilesAcc CurrentFile MaxFile TweetsFolder Tweets nil}
      end
      proc {ThreadsParser P S}
         case S of nil then {Send P done}
         [] H|T then {Send P {SplitLine H}} {ThreadsParser P T}
         end
      end
      S
   in
      if CurrentThread =< N then
         thread
            S = {TheadsReadNFiles 1 {List.length {List.nth Tweets CurrentThread}} TweetsFolder {List.nth Tweets CurrentThread}}
         end
         thread {ThreadsParser Port S} end
         {ThreadsCreation Port N CurrentThread+1 NbFiles TweetsFolder Tweets}
      end
   end
   in
      local TweetsFolder Tweets L Out in
         TweetsFolder = {GetSentenceFolder}
         Tweets = {OS.getDir TweetsFolder}
         L = {SplitIntoN Tweets N nil 1 {List.length Tweets}}
         {ThreadsCreation Port N 1 {List.length Tweets} TweetsFolder L}
      end
   end
   
   %%%% Ajouter vos fonctions et procedures auxiliaires ici

   %% Procedure qui permet de separer les mots d'une phrase
   proc {SplitLine Sentence ?R}
      fun {F String Liste Words}
         case String
         of nil then
            if Words == nil then Liste else {Append Liste [Words]} end
         [] H|T then
            if (H == 226) andthen T.1 == 128 then
               if Words == nil then {F T Liste nil}
               elseif H == 8217 then {F T Liste Words}
               else {F T {Append Liste [Words]} nil}
               end
            else
               case {Char.type H}
               of lower then {F T Liste {Append Words [H]}}
               [] upper then {F T Liste {Append Words [{Char.toLower H}]}}
               [] digit then {F T Liste {Append Words [H]}}
               [] punct then
                  if H == 38 then {F T.2.2.2.2 Liste nil}
                  else
                     if Words == nil then {F T {Append Liste [nil]} nil}
                     else
                        {F T {Append {Append Liste [Words]} [nil]} nil}
                     end
                  end
               else
                  if Words == nil then {F T Liste nil}
                  elseif H == 8217 then {F T Liste Words}
                  else {F T {Append Liste [Words]} nil}
                  end
               end
            end
         else
            nil
         end
      end
   in 
      R = {F Sentence nil nil}
   end

   %%% Fonction qui permet de lire les fichiers et d'en extraire les lignes
   fun {Scan IF Out}
      Is={IF getS($)}  
   in 
      if Is==false then
         {IF close}
         Out
      else 
         {Scan IF {List.append Out [Is]}}
      end 
   end 

   %Fonction qui permet de séparer une liste en listes de N éléments
   fun {SplitIntoN L N Acc I Len}
      if I == N then {List.append Acc [L]}
      else
            local A B in
               {List.takeDrop L Len div N A B }
               {SplitIntoN B N {List.append Acc [A]} I+1 Len}
            end
      end
   end

   %%% Procedure qui permet de recuperer les resultats des threads de parsing et les transforme en arbre
   proc {StreamToTree S NbThreads ?R}
      fun {StreamToTreeAcc S Tree NbThreads NbFinished}
         case S
         of H|T then
            if H == done then
               if NbFinished == NbThreads then Tree
               else {StreamToTreeAcc T Tree NbThreads NbFinished+1}
               end
            else {StreamToTreeAcc T {AddToTree Tree H} NbThreads NbFinished}
            end
         end  
      end
   in 
      R = {StreamToTreeAcc S leaf NbThreads-1 0}
   end

   %%% Fonction qui permet d'ajouter un mot a l'arbre
   fun {AddToTree Tree List}
      proc {AddWord Tree Word NextWord Predictable ?R}
         fun {StrToAtom Word} if {Atom.is Word} then Word else {String.toAtom Word} end end
         fun {F Tree Word NextWord Predictable}
            if Word == nil orelse NextWord == nil orelse Predictable == nil then Tree
            else 
               local A1 A2 in
                  A1 = {Lookup Word Tree}
                  case A1 
                  of leaf then 
                     A2 = {Insert Word {Insert NextWord {Insert Predictable 1 leaf} leaf} Tree}
                     A2
                  [] tree(key:K value :V T1 T2) then
                     local B1 B2 B3 in
                        B1 = {Lookup NextWord A1}
                        case B1
                        of leaf then
                           B2 = {Insert NextWord {Insert Predictable 1 leaf} A1}
                           B3 = {Insert Word B2 Tree}
                           B3
                        [] tree(key:K value :V T1 T2) then
                           local C1 C2 C3 C4 in
                              C1 = {Lookup Predictable B1}
                              if C1 == leaf then
                                 C4 = {Insert Predictable 1 B1}
                                 C3 = {Insert NextWord C4 A1}
                                 C2 = {Insert Word C3 Tree}
                                 C2
                              else 
                                 C4 = {Insert Predictable {String.toInt {VirtualString.toString C1}}+1 B1}
                                 C3 = {Insert NextWord C4 A1}
                                 C2 = {Insert Word C3 Tree}
                                 C2
                              end
                           end
                        end
                     end
                  end
               end
            end
         end
      in
         R = {F Tree {StrToAtom Word} {StrToAtom NextWord} {StrToAtom Predictable}}
      end
   in
      case List
      of nil then Tree
      [] Word|T then
         case T of nil then Tree
         [] NextWord|T then
            case T of nil then Tree
            [] Predictable|_ then {AddToTree {AddWord Tree Word NextWord Predictable} List.2}
            end
         end
      end
   end

   %%% Fonction qui retourne les 2 derniers mots d'une liste de mots
   proc {GetLastWords L ?R}
      fun {GetLast2 L}
         if L == nil then nil
         elseif {List.length L} == 1 then nil
         else [{String.toAtom L.2.1} {String.toAtom L.1}]
         end
      end 
   in
      R = {GetLast2 {List.reverse {SplitLine L}}}
   end

   %%% Fonction qui ajoute une valeur a un arbre
   fun {Insert K W T}
      case T
      of leaf then tree(key:K value:W leaf leaf)
      [] tree(key:Y value:V T1 T2) andthen K==Y then
          tree(key:K value:W T1 T2)
      [] tree(key:Y value:V T1 T2) andthen K<Y then
          tree(key:Y value:V {Insert K W T1} T2)
      [] tree(key:Y value:V T1 T2) andthen K>Y then
          tree(key:Y value:V T1 {Insert K W T2})
      end
   end
   
   %%% Fonction qui retourne la valeur associee a une cle dans un arbre
   fun {Lookup K T}
      case T
      of leaf then leaf
      [] tree(key:Y value:V T1 T2) andthen K==Y then V
      [] tree(key:Y value:V T1 T2) andthen K<Y then {Lookup K T1}
      [] tree(key:Y value:V T1 T2) andthen K>Y then {Lookup K T2}
      end
   end

   %%% Fonction qui parcours un arbre en profondeur et retourne une liste de listes de mots et de leur probabilite
   fun {DFS T Acc}
      case T
      of leaf then Acc
      [] tree(key:K value:V T1 T2) then
          local X Y Z in
              X = {List.append Acc [[[K] V]]}
              Y = {DFS T1 X}
              Z = {DFS T2 Y}
              Z
          end
      end
   end

   %%% Fonction qui retourne le mot le plus probable dans un arbre
   fun {GetMostProbableWord L Acc}
      case L
      of nil then Acc
      [] H|T then
         if {String.toInt {VirtualString.toString H.2.1}} > {String.toInt {VirtualString.toString Acc.2.1}} then
            {GetMostProbableWord T H}
         elseif {String.toInt {VirtualString.toString H.2.1}} == {String.toInt {VirtualString.toString Acc.2.1}} then
            {GetMostProbableWord T [{List.sort {List.append Acc.1 [H.1.1]} Value.'<'} H.2.1]}
         else
            {GetMostProbableWord T Acc}
         end
      end
   end

   %%% Procedure qui declenche la prediction
   proc {ActionFetch}
      local Var in
         Var = {Press}
         case Var.1.1
         of nil then {OutputText set(1:"Prédiction : / \nFréquence : /")}
         [] _ then {OutputText set(1:"Prédiction : "#Var.1.1#"\nFréquence : "#Var.2.1)}
         end
      end
   end

   %%%% Fetch Tweets Folder from CLI Arguments
   %%%% See the Makefile for an example of how it is called
   fun {GetSentenceFolder}
      Args = {Application.getArgs record('folder'(single type:string optional:false))}
   in
      Args.'folder'
   end

   %%%% Decomnentez moi si besoin
   %proc {ListAllFiles L}
   %   case L of nil then skip
   %   [] H|T then {Browse {String.toAtom H}} {ListAllFiles T}
   %   end
   %end
    
   %%%% Procedure principale qui cree la fenetre et appelle les differentes procedures et fonctions
   proc {Main}
      TweetsFolder = {GetSentenceFolder}
   in
      %%% Fonction d'exemple qui liste tous les fichiers
      %%% contenus dans le dossier passe en Argument.
      %%% Inspirez vous en pour lire le contenu des fichiers
      %%% se trouvant dans le dossier
      %%%% N'appelez PAS cette fonction lors de la phase de
      %%%% soumission !!!
      % {ListAllFiles {OS.getDir TweetsFolder}}
       
      local NbThreads SeparatedWordsPort in
	      {Property.put print foo(width:1000 depth:1000)}  % for stdout siz
      
         % TODO
      
         % Creation de l interface graphique
         Description=td(
            title: "Text predictor"
            lr(text(handle:InputText width:50 height:10 background:white foreground:black wrap:word) button(text:"Predict" width:15 action:ActionFetch))
            text(handle:OutputText width:50 height:10 background:black foreground:white glue:w wrap:word)
            action:proc{$}{Application.exit 0} end % quitte le programme quand la fenetre est fermee
            )
      
         % Creation de la fenetre
         Window={QTk.build Description}
         {Window show}
      
         {InputText tk(insert 'end' "Loading... Please wait.")}
         {InputText bind(event:"<Control-s>" action:Press)} % You can also bind events
         
                  % On lance les threads de lecture et de parsing
         SeparatedWordsPort = {NewPort SeparatedWordsStream}
         NbThreads = 4

         if(NbThreads>{List.length {OS.getDir TweetsFolder}}) then
            {LaunchThreads SeparatedWordsPort {List.length {OS.getDir TweetsFolder}}}
            Data = {StreamToTree SeparatedWordsStream {List.length {OS.getDir TweetsFolder}}}
         else 
            {LaunchThreads SeparatedWordsPort NbThreads}
            Data = {StreamToTree SeparatedWordsStream NbThreads}
         end
         
         {InputText set(1:"")}
      end
   %%ENDOFCODE%%
   end
   % Appelle la procedure principale
   {Main}
end
